<%--
  Created by IntelliJ IDEA.
  User: hoang
  Date: 6/15/2019
  Time: 7:42 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Annex - Responsive Bootstrap 4 Admin Dashboard</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="Mannatthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/dashboard/images/favicon.ico">

    <link href="${pageContext.request.contextPath}/assets/dashboard/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/assets/dashboard/css/icons.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/assets/dashboard/css/style.css" rel="stylesheet" type="text/css">

</head>


<body class="fixed-left">

<!-- Begin page -->
<div class="accountbg"></div>
<div class="wrapper-page">

    <div class="card">
        <div class="card-block">

            <div class="ex-page-content text-center">
                <h1 class="">404!</h1>
                <h3 class="">Sorry, page not found</h3><br>

                <a class="btn btn-danger mb-5 waves-effect waves-light" href="/admin/dashboard">Back to Dashboard</a>
            </div>

        </div>
    </div>
</div>


<!-- jQuery  -->
<script src="${pageContext.request.contextPath}/assets/dashboard/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/modernizr.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/detect.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/fastclick.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/jquery.slimscroll.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/jquery.blockUI.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/waves.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/jquery.nicescroll.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/jquery.scrollTo.min.js"></script>

<!-- App js -->
<script src="${pageContext.request.contextPath}/assets/dashboard/js/app.js"></script>

</body>
</html>
