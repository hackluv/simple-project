<%@ page import="java.util.HashMap" %><%--
  Created by IntelliJ IDEA.
  User: hoang
  Date: 6/11/2019
  Time: 10:37 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Admin Dashboard Login</title>
    <meta content="Admin Dashboard" name="description"/>
    <meta content="Mannatthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/dashboard/images/favicon.ico">

    <link href="${pageContext.request.contextPath}/assets/dashboard/css/bootstrap.min.css" rel="stylesheet"
          type="text/css">
    <link href="${pageContext.request.contextPath}/assets/dashboard/css/icons.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/assets/dashboard/css/style.css" rel="stylesheet" type="text/css">

</head>


<body>

<!-- Begin page -->
<div class="accountbg"></div>
<div class="wrapper-page">

    <div class="card">
        <div class="card-body">

            <h3 class="text-center mt-0 m-b-15">
                <a href="/login" class="logo logo-admin"><img
                        src="${pageContext.request.contextPath}/assets/dashboard/images/logo.png" height="24"
                        alt="logo"></a>
            </h3>

            <div class="p-3">
                <form class="form-horizontal m-t-20" action="/login" method="post" enctype="application/json">

                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="text" required="" placeholder="Email" name="email">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="password" required="" placeholder="Password"
                                   name="password">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Remember me</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-center row m-t-20">
                        <div class="col-12">
                            <button class="btn btn-danger btn-block waves-effect waves-light" type="submit">Log In
                            </button>
                        </div>
                    </div>

                    <%--                    <div class="form-group m-t-10 mb-0 row">--%>
                    <%--                        <div class="col-sm-7 m-t-20">--%>
                    <%--                            <a href="pages-recoverpw.html" class="text-muted"><i class="mdi mdi-lock"></i> <small>Forgot your password ?</small></a>--%>
                    <%--                        </div>--%>
                    <%--                        <div class="col-sm-5 m-t-20">--%>
                    <%--                            <a href="pages-register.html" class="text-muted"><i class="mdi mdi-account-circle"></i> <small>Create an account ?</small></a>--%>
                    <%--                        </div>--%>
                    <%--                    </div>--%>
                </form>
            </div>

        </div>
    </div>
</div>


<!-- jQuery  -->
<script src="${pageContext.request.contextPath}/assets/dashboard/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/modernizr.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/waves.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/jquery.slimscroll.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/jquery.nicescroll.js"></script>
<script src="${pageContext.request.contextPath}/assets/dashboard/js/jquery.scrollTo.min.js"></script>

<!-- App js -->
<script src="${pageContext.request.contextPath}/assets/dashboard/js/app.js"></script>
</body>
</html>