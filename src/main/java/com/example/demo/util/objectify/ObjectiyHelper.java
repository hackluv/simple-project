package com.example.demo.util.objectify;

import com.example.demo.entity.Account;
import com.example.demo.entity.Category;
import com.example.demo.entity.Member;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ObjectiyHelper implements ServletContextListener {
    /**
     * Register Class with ObjectifyService
     */
    public void contextInitialized(ServletContextEvent event) {
        ObjectifyService.register(Member.class);
        ObjectifyService.register(Account.class);
        ObjectifyService.register(Category.class);
    }
}
