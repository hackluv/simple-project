package com.example.demo.entity;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
public class Account {
    @Id
    private long id;
    private String username;
    private String password;
    private String salt;
    @Index
    private String email;
    private int role;
    private String avatar;
    @Index
    private int status;
    private long createdAtMLS;
    private long updatedAtMLS;
    @Load
    transient Ref<Member> refMember;

    public Account() {
    }

    @Index
    public enum Status {
        ACTIVE(1), DEACTIVE(0), DELETED(-1);

        int value;

        Status(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Status findByValue(int value) {
            for (Status status :
                    Status.values()) {
                if (status.getValue() == value) {
                    return status;
                }
            }
            return null;
        }
    }

    public enum Role {
        ADMIN(2), MEMBER(1);

        int value;

        Role(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Role findByValue(int value) {
            for (Role role :
                    Role.values()) {
                if (role.getValue() == value) {
                    return role;
                }
            }
            return null;
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        if (status == null) {
            status = Status.DEACTIVE;
        }
        this.status = status.getValue();
    }

    public int getRole() {
        return role;
    }

    public void setRole(Role role) {
        if (role == null) {
            role = Role.MEMBER;
        }
        this.role = role.getValue();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getCreatedAtMLS() {
        return createdAtMLS;
    }

    public void setCreatedAtMLS(long createdAtMLS) {
        this.createdAtMLS = createdAtMLS;
    }

    public long getUpdatedAtMLS() {
        return updatedAtMLS;
    }

    public void setUpdatedAtMLS(long updatedAtMLS) {
        this.updatedAtMLS = updatedAtMLS;
    }

    public Ref<Member> getRefMember() {
        return refMember;
    }

    public void setRefMember(Ref<Member> refMember) {
        this.refMember = refMember;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
