package com.example.demo.entity;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Product {
    @Id
    private long id;
    @Index
    private String title;
    private String image;
    Ref<Category> categoryRef;
    private long createdAt;
    private long updatedAt;
    @Index
    private int status;

    @Index
    public enum Status {
        ACTIVE(1), DEACTIVE(0), DELETED(-1);

        int value;

        Status(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Product.Status findByValue(int value) {
            for (Product.Status status :
                    Product.Status.values()) {
                if (status.getValue() == value) {
                    return status;
                }
            }
            return null;
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(Product.Status status) {
        if (status == null) {
            status = Product.Status.DEACTIVE;
        }
        this.status = status.getValue();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Ref<Category> getCategoryRef() {
        return categoryRef;
    }

    public void setCategoryRef(Ref<Category> categoryRef) {
        this.categoryRef = categoryRef;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }
}
