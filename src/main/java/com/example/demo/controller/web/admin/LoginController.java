package com.example.demo.controller.web.admin;

import com.example.demo.entity.Account;
import com.example.demo.entity.Member;
import com.example.demo.entity.MessageCode;
import com.example.demo.entity.dto.AccountDTO;
import com.example.demo.model.MemberModel;
import com.example.demo.util.generate.ApplicationConstant;
import com.example.demo.util.hash.HashPassword;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginController extends HttpServlet {
    MemberModel model = new MemberModel();
    HashPassword hashPassword = new HashPassword();
    MessageCode message = new MessageCode();
    Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String referrer = req.getHeader("referer");
        System.out.println("Redirect từ trang: " + referrer);
        if (referrer == null) {
            referrer = "/login";
        }
        req.setAttribute("referer", referrer);
        req.getRequestDispatcher("/admin/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setHeader("Cache-Control", "nocache");
        resp.setCharacterEncoding("UTF-8");

        String email = req.getParameter("email");
        String password = req.getParameter("password");

        Account account = model.getAccountByEmail(email);
        Member member = account.getRefMember().get();
        AccountDTO accDTO = new AccountDTO(account, member);
        System.out.println(gson.toJson(accDTO));
        if (account != null) {
            if (hashPassword.md5(password + account.getSalt()).equals(account.getPassword())) {
                if (Account.Status.findByValue(account.getStatus()) == Account.Status.ACTIVE && Account.Role.findByValue(account.getRole()) == Account.Role.ADMIN) {
                    HttpSession session = req.getSession();
                    session.setAttribute(ApplicationConstant.CURRENT_LOGGED_IN, gson.toJson(accDTO));
                    resp.sendRedirect("/admin/dashboard");
                } else if (Account.Status.findByValue(account.getStatus()) == Account.Status.DEACTIVE) {
                    message.setCode(HttpServletResponse.SC_BAD_REQUEST);
                    message.setMessage(ApplicationConstant.LOGIN_PLEASE_ACTIVE);
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    resp.getWriter().println(gson.toJson(message));
                } else if (Account.Status.findByValue(account.getStatus()) == Account.Status.DELETED) {
                    message.setCode(HttpServletResponse.SC_BAD_REQUEST);
                    message.setMessage(ApplicationConstant.LOGIN_BLOCK);
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    resp.getWriter().println(gson.toJson(message));
                }
            } else {
                message.setCode(HttpServletResponse.SC_BAD_REQUEST);
                message.setMessage(ApplicationConstant.LOGIN_WRONG_PASSWORD);
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getWriter().println(gson.toJson(message));
            }
        } else {
            message.setCode(HttpServletResponse.SC_NOT_FOUND);
            message.setMessage(ApplicationConstant.LOGIN_WRONG_EMAIL);
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            resp.getWriter().println(gson.toJson(message));
        }
    }
}
