package com.example.demo.controller.api.member;

import com.example.demo.entity.Account;
import com.example.demo.entity.Member;
import com.example.demo.entity.MessageCode;
import com.example.demo.entity.dto.AccountDTO;
import com.example.demo.model.MemberModel;
import com.example.demo.util.generate.ApplicationConstant;
import com.example.demo.util.hash.HashPassword;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;

public class LoginController extends HttpServlet {
    MemberModel model = new MemberModel();
    HashPassword hashPassword = new HashPassword();
    Gson gson = new Gson();
    MessageCode message = new MessageCode();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        message.setCode(HttpServletResponse.SC_BAD_REQUEST);
        message.setMessage(ApplicationConstant.DO_NOT_PERMISSION);
        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        resp.getWriter().println(gson.toJson(message));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setHeader("Cache-Control", "nocache");
        resp.setCharacterEncoding("utf-8");
        BufferedReader reader = req.getReader();
        Account account = gson.fromJson(reader, Account.class);

        String email = account.getEmail();
        String password = account.getPassword();

        Account account1 = model.getAccountByEmail(email);
        Member member = account1.getRefMember().get();
        AccountDTO accDTO = new AccountDTO(account1, member);

        if (account1 != null) {
            if (hashPassword.md5(password + account1.getSalt()).equals(account1.getPassword())) {
                if (Account.Status.findByValue(account1.getStatus()) == Account.Status.ACTIVE){
                    resp.getWriter().println(gson.toJson(accDTO));
                }else if (Account.Status.findByValue(account1.getStatus()) == Account.Status.DEACTIVE){
                    message.setCode(HttpServletResponse.SC_BAD_REQUEST);
                    message.setMessage(ApplicationConstant.LOGIN_PLEASE_ACTIVE);
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    resp.getWriter().println(gson.toJson(message));
                }else if (Account.Status.findByValue(account1.getStatus()) == Account.Status.DELETED){
                    message.setCode(HttpServletResponse.SC_BAD_REQUEST);
                    message.setMessage(ApplicationConstant.LOGIN_BLOCK);
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    resp.getWriter().println(gson.toJson(message));
                }
            } else {
                message.setCode(HttpServletResponse.SC_BAD_REQUEST);
                message.setMessage(ApplicationConstant.LOGIN_WRONG_PASSWORD);
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getWriter().println(gson.toJson(message));
            }
        } else {
            message.setCode(HttpServletResponse.SC_NOT_FOUND);
            message.setMessage(ApplicationConstant.LOGIN_WRONG_EMAIL);
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            resp.getWriter().println(gson.toJson(message));
        }

    }
}
