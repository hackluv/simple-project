package com.example.demo.controller.api.member;

import com.example.demo.entity.Account;
import com.example.demo.entity.MessageBlob;
import com.example.demo.entity.MessageCode;
import com.example.demo.model.MemberModel;
import com.example.demo.service.ImageService;
import com.example.demo.util.generate.ApplicationConstant;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UploadController extends HttpServlet {
    Gson gson = new Gson();
    MessageCode message = new MessageCode();
    ImageService imageService = new ImageService();
    MemberModel model = new MemberModel();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String blobUploadUrl = imageService.createUploadUrl();
        resp.setStatus(HttpServletResponse.SC_OK);
        resp.setContentType("text/plain");
        message.setCode(HttpServletResponse.SC_OK);
        message.setMessage(gson.toJson(blobUploadUrl));
        resp.setStatus(HttpServletResponse.SC_OK);
        resp.getWriter().println(gson.toJson(message));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        MessageBlob messageBlob = imageService.imageUrl(req, resp);
        resp.setStatus(HttpServletResponse.SC_OK);
        resp.setContentType("application/json");
        String email = req.getParameter("email");
        if (email != null){
            Account account = model.getAccountByEmail(email);
            if (account != null){
                account.setAvatar(messageBlob.getServingUrl());
                boolean check = model.changeAvatar(account);
                if (check == true) {
                    message.setCode(HttpServletResponse.SC_OK);
                    message.setMessage(ApplicationConstant.CHANGE_AVATAR_SUCCESS);
                    resp.setStatus(HttpServletResponse.SC_OK);
                    resp.getWriter().println(gson.toJson(message));
                } else if (check == false) {
                    message.setCode(HttpServletResponse.SC_BAD_REQUEST);
                    message.setMessage(ApplicationConstant.CHANGE_AVATAR_FAILED);
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    resp.getWriter().println(gson.toJson(message));
                }
            }else {
                message.setCode(HttpServletResponse.SC_NOT_FOUND);
                message.setMessage(ApplicationConstant.NOT_FOUND);
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                resp.getWriter().println(gson.toJson(message));
            }
        }
    }
}
