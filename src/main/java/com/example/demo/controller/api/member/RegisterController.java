package com.example.demo.controller.api.member;

import com.example.demo.entity.Account;
import com.example.demo.entity.Member;
import com.example.demo.entity.MessageCode;
import com.example.demo.entity.dto.AccountDTO;
import com.example.demo.model.MemberModel;
import com.example.demo.util.generate.ApplicationConstant;
import com.example.demo.util.generate.Generate;
import com.example.demo.util.hash.HashPassword;
import com.google.gson.Gson;
import com.googlecode.objectify.Ref;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Calendar;

public class RegisterController extends HttpServlet {
    MemberModel model = new MemberModel();
    Generate generate = new Generate();
    HashPassword hashPassword = new HashPassword();
    Gson gson = new Gson();
    MessageCode message = new MessageCode();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        message.setCode(HttpServletResponse.SC_BAD_REQUEST);
        message.setMessage(ApplicationConstant.DO_NOT_PERMISSION);
        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        resp.getWriter().println(gson.toJson(message));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setHeader("Cache-Control", "nocache");
        resp.setCharacterEncoding("utf-8");

        BufferedReader reader = req.getReader();
        AccountDTO account = gson.fromJson(reader, AccountDTO.class);
        Account account1 = new Account();
        Member member = new Member();

        if (existEmail(account.getEmail())) {
            String salt = generate.getSalt(10);
            String hashPwd = hashPassword.md5(account.getPassword() + salt);

            account1.setId(Calendar.getInstance().getTimeInMillis());
            account1.setPassword(hashPwd);
            account1.setEmail(account.getEmail());
            account1.setUsername(account.getUsername());
            account1.setSalt(salt);
            account1.setRole(Account.Role.MEMBER);
            account1.setStatus(Account.Status.ACTIVE);
            account1.setAvatar("https://i.imgur.com/fCNkJOp.jpg");
            account1.setCreatedAtMLS(Calendar.getInstance().getTimeInMillis());
            account1.setUpdatedAtMLS(Calendar.getInstance().getTimeInMillis());

            int gen = 0;
            switch (account.getGender()) {
                case "Female":
                    gen = 0;
                    break;
                case "Male":
                    gen = 1;
                    break;
                case "Others":
                    gen = 2;
                    break;
            }

            member.setId(Calendar.getInstance().getTimeInMillis());
            member.setAddress(account.getAddress());
            member.setPhone(account.getPhone());
            member.setFullname(account.getFullname());
            member.setGender(Member.Gender.findByValue(gen));

            account1.setRefMember(Ref.create(member));
            member.setRefAccount(Ref.create(account1));

            boolean check = model.save(account1, member);

            if (check == true) {
                message.setCode(HttpServletResponse.SC_OK);
                message.setMessage(ApplicationConstant.REGISTER_SUCCESS);
                resp.setStatus(HttpServletResponse.SC_OK);
                resp.getWriter().println(gson.toJson(message));
            } else if (check == false) {
                message.setCode(HttpServletResponse.SC_BAD_REQUEST);
                message.setMessage(ApplicationConstant.REGISTER_FAIL);
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getWriter().println(gson.toJson(message));
            }
        } else {
            message.setCode(HttpServletResponse.SC_BAD_REQUEST);
            message.setMessage(ApplicationConstant.EMAIL_EXIST);
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().println(gson.toJson(message));
        }
    }

    private boolean existEmail(String email) {
        Account account = model.getAccountByEmail(email);
        if (account == null) {
            return true;
        } else {
            return false;
        }
    }
}