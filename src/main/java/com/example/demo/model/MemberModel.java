package com.example.demo.model;

import com.example.demo.entity.Account;
import com.example.demo.entity.Member;

import java.util.ArrayList;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class MemberModel {

    /**
     * @param member
     * @return member
     */
    public boolean save(Account account, Member member) {
        ofy().save().entity(account).now();
        ofy().save().entity(member).now();
        return true;
    }

    public ArrayList<Member> listMember() {
        ArrayList<Member> list = (ArrayList<Member>) ofy().load().type(Member.class).list();
        return list;
    }

    /**
     * @param email
     * @return account
     */
    public Account getAccountByEmail(String email) {
        Account account = ofy().load().type(Account.class).filter("email", email).first().now();

        return account;
    }

    public boolean changeAvatar(Account account){
        ofy().defer().save().entity(account);
        return true;
    }
}
